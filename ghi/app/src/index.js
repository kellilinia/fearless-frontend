import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  // line 23 added -> checks if the response is okay
  if (response.ok) {
    // line 25 added -> if okay, then your code gets the data from the responses
    const data = await response.json();
    // console.log modified to print data variable -> then, it prints the data to the console.
    console.log(data);
    root.render(
      <React.StrictMode>
        <App attendees={data.attendees} />
      </React.StrictMode>
    );
    // lines 29-31 added -> If the response is not okay, it prints the response as an error.
  } else {
    console.error(response);
  }
}
loadAttendees();
