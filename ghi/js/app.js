function createCard(name, location, description, pictureUrl, starts, ends) {
    return `
    <div class="grid gap-3 col-5">
    <div class="shadow-sm p-3 mb-5 bg-body-tertiary rounded">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-subtitle mb-2 text-muted">${location}</p>
          <p class="card-text">${description}</p>
          <div class="card-footer">
          ${starts} - ${ends}
        </div>
        </div>
      </div>
      </div>

    `;
}
// Added location as a parameter to createCard on line 1
// Added line 9 for location


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.log("An error has occured while fetching data")
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    // variable location below added:
                    const location = details.conference.location.name;
                    const starts = new Date(details.conference.starts).toLocaleDateString();
                    const ends = new Date(details.conference.ends).toLocaleDateString();
                    // added location as a parameter to html variable
                    const html = createCard(name, location, description, pictureUrl, starts, ends, response);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }

        }
    } catch (e) {
        console.error(e);
        // Line 60-68 added for error handling
        // Creates html file:
        const errorHandling =
            `
            <div class="alert alert-warning" role="alert">
            Error
            </div>
            `;
        // adds html content to browser:
        const column = document.querySelector('.row');
        column.innerHTML += errorHandling;
    }

});
